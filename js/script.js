var Game = function () {
    this.computerPlay = [];
    this.round =  1;
    this.score = 0;
    this.playerGoes = false;
    this.difficulty = $('input[name=difficulty]:checked').val();
};

var start = function() {
    var thisTurn = new Game();

    $( '.wedge' ).unbind( 'click' );
    $('.difficulty').hide();

    thisTurn.newGame();
    console.log('thisTurn is ' + thisTurn);
    console.log('difficulty level is ' + thisTurn.difficulty);
};


function play(file) {
    var embed = document.createElement("embed");
 
    embed.setAttribute('src', file);
    embed.setAttribute('hidden', true);
    embed.setAttribute('autostart', true);
 
    document.body.appendChild(embed);
};

Game.prototype.newGame = function() {
    console.log(this);
    this.round = 1;
    this.score = 0;
    this.computerPlay.length = 0;
    $( '.wedge' ).unbind( 'click' );
    this.computerPlayGenerate();
    this.displayComputerPlay();

};

Game.prototype.newRound = function() {
    $( '.wedge' ).unbind( 'click' );
    this.computerPlayGenerate();
    this.displayComputerPlay();
};

Game.prototype.computerPlayGenerate = function() {
    // for (i=0; i < this.round; i++) {
        var currentRound = Math.floor((Math.random()*4)+1);
        console.log(currentRound);
        this.computerPlay.push(currentRound);
        console.log(this.computerPlay);
    // }
};
    
Game.prototype.displayComputerPlay = function(counter) {
    var roundCounter = counter || 0;
    var that = this;
    var speed = (this.difficulty * 100);

    if (roundCounter < this.round) {
   
        var wedgeSelect = that.computerPlay[roundCounter];
        that.wedgeEventHandler(wedgeSelect);

        roundCounter++;


        setTimeout(function() {that.displayComputerPlay(roundCounter)}, speed);

        if (roundCounter === that.round ) {
            that.userPlayGenerate();
        }
    }

};


Game.prototype.wedgeEventHandler = function(wedgeToPlay) {
    switch(wedgeToPlay) {
        case 1:
            $('.blue').animate({ opacity: 1 }, 100);
            play('../oscar/sounds/mp3/sounds_01.mp3');
            $('.blue').animate({ opacity: .5 }, 100);
            break;
        case 2:
            $('.yellow').animate({ opacity: 1 }, 100);
            play('../oscar/sounds/mp3/sounds_02.mp3');
            $('.yellow').animate({ opacity: .5 }, 100);
            break;
        case 3:
            $('.red').animate({ opacity: 1 }, 100);
            play('../oscar/sounds/mp3/sounds_03.mp3');
            $('.red').animate({ opacity: .5 }, 100);
            break;
        case 4:
            $('.green').animate({ opacity: 1 }, 100);
            play('../oscar/sounds/mp3/sounds_04.mp3');
            $('.green').animate({ opacity: .5 }, 100);
            break;
    }
};



Game.prototype.userPlayGenerate = function(counter) {
    var turnCounter = counter || 0;

    this.playerGoes = true;

    var that = this;

    $('.wedge').click(function() {
        var userWedgeSelect = $(this).data('piece');
        console.log(userWedgeSelect);
        that.wedgeEventHandler(userWedgeSelect);


        if (userWedgeSelect === that.computerPlay[turnCounter]) {
            that.keepScore();
            $('#scoreDisplay').html(that.score);
            turnCounter++;

            if (turnCounter < that.round) {
                $( '.wedge' ).unbind( 'click' );
                that.userPlayGenerate(turnCounter);
            } else {
                that.round++;
                setTimeout(function() {that.newRound()}, 1000);
            }


        } else {
            $( '.wedge' ).unbind( 'click' );
            that.wrongAnswer();

        }

    })

};


Game.prototype.wrongAnswer = function() {
    var number = this.turn
    // this.wedgeEventHandler(number);
    // this.wedgeEventHandler(number);
};



Game.prototype.keepScore =  function() {
    var number = this.score;
    var newScore = number + $(this).data('rate');
    this.score = newScore;
};




$(document).ready(function() {
    $('button').click(start);

}); 


